# Livi Counter

API to return seconds until move out! Reminder that time is precious and brief, spend it wisely.

### Launching
```bash
go run main.go
```