package main

import (
	"errors"
	"github.com/gin-gonic/gin"
	"math"
	"net/http"
	"time"
)

type child struct {
	ID      string    `json:"id"`
	Name    string    `json:"name"`
	Exit    time.Time `json:"exit"`
	Seconds int       `json:"seconds"`
}

var children = []child{
	{ID: "1", Name: "Ogden", Exit: time.Date(2039, 3, 31, 2, 0, 0, 0, time.UTC), Seconds: 0},
	{ID: "2", Name: "Olivia", Exit: time.Date(2040, 2, 25, 2, 0, 0, 0, time.UTC), Seconds: 0},
}

func getChildById(id string, context *gin.Context) (*child, error) {
	for i, c := range children {
		if c.ID == id {
			// Return the child object at that position in the children struct, and pass nil back as the error
			return &children[i], nil
		}
	}
	// If no matches are found in the for loop, return nil as the child and return an error indicating so
	return nil, errors.New("child not found")
}

func getChildren(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, children)
}

func getChild(context *gin.Context) {
	id := context.Param("id")
	child, err := getChildById(id, context)

	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "child not found"})
		return
	}

	context.IndentedJSON(http.StatusOK, child)
}

func calculateSeconds(start time.Time, end time.Time) int {
	difference := math.Round(start.Sub(end).Seconds())
	seconds := int(difference)
	return seconds
}

func main() {
	now := time.Now()
	for i, c := range children {
		seconds := calculateSeconds(c.Exit, now)
		p := &children[i].Seconds
		*p = seconds
	}

	router := gin.Default()
	router.GET("/children", getChildren)
	router.GET("/children/:id", getChild)
	router.Run("localhost:9090")
}
